﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace AppTesteMediaBox.Controls
{
    public sealed partial class AudioMessageBox : UserControl
    {
        private static DependencyProperty thumbnailProperty = DependencyProperty.Register("Thumbnail", typeof(string), typeof(AudioMessageBox), new PropertyMetadata(string.Empty, ThumbnailChanged));
        private static DependencyProperty hourProperty = DependencyProperty.Register("Hour", typeof(string), typeof(AudioMessageBox), new PropertyMetadata(string.Empty, HourChanged));

        public static DependencyProperty ThumbnailProperty
        {
            get
            {
                return thumbnailProperty;
            }
        }

        public static DependencyProperty HourProperty
        {
            get
            {
                return hourProperty;
            }
        }

        public string Thumbnail
        {
            get
            {
                return GetValue(ThumbnailProperty).ToString();
            }

            set
            {
                SetValue(ThumbnailProperty, value);
            }
        }

        public string Hour
        {
            get
            {
                return GetValue(HourProperty).ToString();
            }

            set
            {
                SetValue(HourProperty, value);
            }
        }

        private DispatcherTimer dispatcherTimer;
        private bool firstplay = true;

        public AudioMessageBox()
        {
            this.InitializeComponent();
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick +=dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            

        }

        private void dispatcherTimer_Tick(object sender, object e)
        {
            sld.Value = tbAudioFile.Position.Seconds;
            TimeSpan ts = tbAudioFile.Position;
            txtAudioTime.Text = String.Format("{0:mm\\:ss}", ts); 
        }



        private static void ThumbnailChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as AudioMessageBox;

            if (control != null)
            {
                control.tbAudioFile.Source = new Uri("ms-appx:///" + e.NewValue.ToString());
            }
        }

        private static void HourChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as AudioMessageBox;

            if (control != null)
            {
                control.tbHour.Text = e.NewValue.ToString();
            }
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if(firstplay)
            {
                dispatcherTimer.Start();
                firstplay = false;
            }
            // if(tbAudioFile.IsPaused)
            if (tbAudioFile.CurrentState == MediaElementState.Playing)
            {
                if (tbAudioFile.CanPause)
                {
                    tbAudioFile.Pause();
                    imgPlay.Visibility = Visibility.Visible;
                    imgPause.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                tbAudioFile.Play();
                imgPlay.Visibility = Visibility.Collapsed;
                imgPause.Visibility = Visibility.Visible;

            }
        }

        private void tbAudioFile_MediaOpened(object sender, RoutedEventArgs e)
        {
            TimeSpan ts = tbAudioFile.NaturalDuration.TimeSpan;
            txtAudioTime.Text = String.Format("{0:mm\\:ss}", ts); 
            sld.Maximum = ts.TotalSeconds;
            sld.SmallChange = 1;
            sld.LargeChange = Math.Min(10, ts.Seconds / 10);
        }

        private void seekBar_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            tbAudioFile.Position = TimeSpan.FromSeconds(sld.Value);
        }

        private void timelineSlider_ValueChanged(object sender, RoutedEventArgs e)
        {
            tbAudioFile.Position = TimeSpan.FromSeconds(sld.Value);

        }

        private void tbAudioFile_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            sld.Value = tbAudioFile.Position.Seconds;
        }
    }
}
