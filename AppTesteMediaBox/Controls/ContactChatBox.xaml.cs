﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoldingHabitsWU.Controls
{
    public sealed partial class ContactChatBox : UserControl
    {
        private static DependencyProperty messageProperty = DependencyProperty.Register("Message", typeof(string), typeof(ContactChatBox), new PropertyMetadata(string.Empty, MessageChanged));
        private static DependencyProperty hourProperty = DependencyProperty.Register("Hour", typeof(string), typeof(ContactChatBox), new PropertyMetadata(string.Empty, HourChanged));

        public static DependencyProperty MessageProperty
        {
            get
            {
                return messageProperty;
            }
        }

        public static DependencyProperty HourProperty
        {
            get
            {
                return hourProperty;
            }
        }

        public string Message
        {
            get
            {
                return GetValue(MessageProperty).ToString();
            }

            set
            {
                SetValue(MessageProperty, value);
            }
        }

        public string Hour
        {
            get
            {
                return GetValue(HourProperty).ToString();
            }

            set
            {
                SetValue(HourProperty, value);
            }
        }

        public ContactChatBox()
        {
            this.InitializeComponent();
        }

        private static void MessageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as ContactChatBox;

            if (control != null)
            {
                control.tbMessage.Text = e.NewValue.ToString();
            }
        }

        private static void HourChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as ContactChatBox;

            if (control != null)
            {
                control.tbHour.Text = e.NewValue.ToString();
            }
        }
    }
}
