﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace AppTesteMediaBox.Controls
{

    public sealed partial class ImageMessageBox : UserControl
    {
        private static DependencyProperty thumbnailProperty = DependencyProperty.Register("Thumbnail", typeof(string), typeof(ImageMessageBox), new PropertyMetadata(string.Empty, ThumbnailChanged));
        private static DependencyProperty hourProperty = DependencyProperty.Register("Hour", typeof(string), typeof(ImageMessageBox), new PropertyMetadata(string.Empty, HourChanged));

        public static DependencyProperty ThumbnailProperty
        {
            get
            {
                return thumbnailProperty;
            }
        }

        public static DependencyProperty HourProperty
        {
            get
            {
                return hourProperty;
            }
        }

        public string Thumbnail
        {
            get
            {
                return GetValue(ThumbnailProperty).ToString();
            }

            set
            {
                SetValue(ThumbnailProperty, value);
            }
        }

        public string Hour
        {
            get
            {
                return GetValue(HourProperty).ToString();
            }

            set
            {
                SetValue(HourProperty, value);
            }
        }

        public ImageMessageBox()
        {
            this.InitializeComponent();
        }

        private static void ThumbnailChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as ImageMessageBox;

            if (control != null)
            {
                BitmapImage bmp = new BitmapImage(new Uri("ms-appx:///" + e.NewValue.ToString()));
                control.tbThumbnail.Source = bmp;
            }
        }

        private static void HourChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as ImageMessageBox;

            if (control != null)
            {
                control.tbHour.Text = e.NewValue.ToString();
            }
        }
    }

}
