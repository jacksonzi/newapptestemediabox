﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoldingHabitsWU.Controls
{
    public sealed partial class UserChatBox : UserControl
    {
        private static DependencyProperty messageProperty = DependencyProperty.Register("Message", typeof(string), typeof(UserChatBox), new PropertyMetadata(string.Empty, MessageChanged));
        private static DependencyProperty hourProperty = DependencyProperty.Register("Hour", typeof(string), typeof(UserChatBox), new PropertyMetadata(string.Empty, HourChanged));

        public static DependencyProperty MessageProperty
        {
            get
            {
                return messageProperty;
            }
        }

        public static DependencyProperty HourProperty
        {
            get
            {
                return hourProperty;
            }
        }

        public string Message
        {
            get
            {
                return GetValue(MessageProperty).ToString();
            }

            set
            {
                SetValue(MessageProperty, value);
            }
        }

        public string Hour
        {
            get
            {
                return GetValue(HourProperty).ToString();
            }

            set
            {
                SetValue(HourProperty, value);
            }
        }

        public UserChatBox()
        {
            this.InitializeComponent();
        }

        private static void MessageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as UserChatBox;

            if (control != null)
            {
                control.tbMessage.Text = e.NewValue.ToString();
            }
        }

        private static void HourChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as UserChatBox;

            if (control != null)
            {
                control.tbHour.Text = e.NewValue.ToString();
            }
        }
    }
}
